//
//  ViewController.swift
//  Test2Wearables
//
//  Created by Vivek Batra on 2019-11-07.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit
import Particle_SDK


class ViewController: UIViewController
{
    
    let USERNAME = "vivekbatra456@gmail.com"
    let PASSWORD = "mightbeiamwrong"
    let DEVICE_ID = "220026000f47363333343437"
    var myPhoton : ParticleDevice?
    
    
    var timerObj: Timer?
    var time = 0.0
    var counter = -1
    var numberOfMohammad = 0.0
    var bol : Bool = false
    
    
    @IBOutlet weak var printTimeLabel: UILabel!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var sliderLabel: UILabel!
    
   
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
         ParticleCloud.init()
        ParticleCloud.sharedInstance().login(withUser: self.USERNAME, password: self.PASSWORD) { (error:Error?) -> Void in
            if (error != nil)
            {
                print("Wrong credentials or as! ParticleCompletionBlock no internet connectivity, please try again")
                print(error?.localizedDescription)
            }
            else
            {
                print("Login success!")
                self.getDeviceFromCloud()
            }
        }
    }
    
    func getDeviceFromCloud()
    {
        ParticleCloud.sharedInstance().getDevice(self.DEVICE_ID)
        { (device:ParticleDevice?, error:Error?) in
            
            if (error != nil)
            {
                print("Could not get device")
                print(error?.localizedDescription)
                return
            }
            else
            {
                print("Got photon from cloud: \(device?.id)")
                self.myPhoton = device
            }
        }
    }

    
    
    
    @IBAction func startButtonPressed(_ sender: Any)
    {
            timerObj?.invalidate()
            let seconds = 1.0
            timerObj = Timer.scheduledTimer(timeInterval: seconds, target: self, selector: #selector(timerHandler(_:)), userInfo: nil, repeats: true)
    }
    @objc func timerHandler(_ timer: Timer)
    {
        sliderLabel.text = ("Slow down by: \(numberOfMohammad)")
        if (numberOfMohammad != 0)
        {
            time = time + 1 - Double(self.numberOfMohammad)
            if(time > 3.9 || time > 7.9 || time > 11.9 || time > 15.9)
            {
                bol = true
                print(bol)
            }
            print(time)
        }
        else
        {
            time = time + 1
            bol = true
        }
        printTimeLabel.text = "\(Int(time))"

        if(Int(time) == 1)
        {
            let parameters = [""]
            var task = self.myPhoton!.callFunction("initialFace", withArguments: parameters)
            {
                (resultCode : NSNumber?, error : Error?) -> Void in
                if (error == nil)
                {
                    print("sent to particle")
                }
                else
                {
                    print("was not sent to particle")
                }
            }
        }
        if (Int(time) % 4 == 0 && Int(time) < 20 && bol == true)
        {
            counter = counter + 1
            bol = false
            let parameters = ["\(counter)"]
            var task = self.myPhoton!.callFunction("startFace", withArguments: parameters)
            {
                (resultCode : NSNumber?, error : Error?) -> Void in
                if (error == nil)
                {
                    print("sent to particle")
                }
                else
                {
                    print("was not sent to particle")
                }
            }
        }
        
        if (Int(time) == 20)
        {
            let parameters = [""]
            var task = self.myPhoton!.callFunction("lastface", withArguments: parameters)
            {
                (resultCode : NSNumber?, error : Error?) -> Void in
                if (error == nil)
                {
                    print("sent to particle")
                }
                else
                {
                    print("was not sent to particle")
                }
            }
        }
    }
    
    @IBAction func sliderMoved(_ sender: Any)
    {
        self.numberOfMohammad = Double((slider.value))
    }
    

}

