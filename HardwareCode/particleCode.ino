// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>
InternetButton button = InternetButton();


void setup()
{
    button.begin();
    Particle.function("startFace", showFace);
    Particle.function("initialFace", turnAllLightsGreen);
    Particle.function("lastface", turnAllLightsRed);
    Particle.function("turnOffAllLeds", turnAllLightsOFF);
    button.allLedsOff();
}

void loop()
{

}

int turnAllLightsOFF(String cmd)
{
    button.allLedsOff();
    return 1;
}


int turnAllLightsGreen(String cmd)
{
    for(int i = 3; i <= 9; i++)
    {
        button.ledOn(i, 0, 255, 0);
    }
        return 1;
}


int showFace(String cmd)
{
    int j = cmd.toInt();
    button.ledOff(3 + j);
    button.ledOff(9 - j);
    return 1;
}

int turnAllLightsRed(String cmd)
{
    button.allLedsOn(255, 0, 0);
    return 1;
}
